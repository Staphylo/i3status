i3status
========

Install
-------

To make this status bar work you should install dbus plugin for php and enable
it. The two following commands launched as root are doing it for you.

###
pecl install dbus-beta
echo "extension=dbus.so" >> /etc/php/php.ini
###

Then move this folder to a good place such as `~/.i3/status`
To finish the installation just set the `status_command ~/.i3/status/status.php`
in the section bar in your `~/.i3/config`

if the option open_basedir is set in php.ini make sure it can access to `/proc`

make sure that status.php is executable

`chmod +x status.php`

Configuration
-------------

To configure the status bar you need to edit the status.php
There is no documentation on the plugin for now which is quite anoying when you
don't know which are the existing functions.

Known bugs
----------

Warning appears due to file missing in `/sys/class/power_supply/BAT0` when the
adapter is removed or inserted.

Each bug are written down in status.log

If you are using this bar and you see a bug, feel free to report it and it will
be corrected as soon as possible.

Screenshots
-----------

![Screenshot 1](http://www.staphylo.org/i3status/screen1.png "Screenshot 1")
![Screenshot 1](http://www.staphylo.org/i3status/screen2.png "Screenshot 1")

Copyright and other shity stuff
-------------------------------

License Beerware (Revision 42)
In other terms do what you want with that ! =)

By Samuel 'Staphylo' Angebault <staphyloa@gmail.com>
