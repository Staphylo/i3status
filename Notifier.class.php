<?php

// Documentation about DBus Notification
// [1] http://developer.gnome.org/notification-spec/
// [2] http://derickrethans.nl/talks/dbus-ipc09.pdf

// Keep status of all notification sent?
// To know if they are still openend or not (could be useless for perpetual 
// notification)
// Get the capabilities of the notifier deamon to know what we can use

class Notifier
{
    private $dbus;
    private $notification;
    private $default;
    private $ok = false;

    public function __construct()
    {
        $this->Connect();
        // Using $res result in a segfault with twmnd
        //$res = $this->notification->GetCapabilities();
        $this->default = array(
            "name"    => "staphtus",
            "id"      => 0,
            "icon"    => '',
            "title"   => '',
            "body"    => '',
            "actions" => array(),
            "x"       => 500,
            "y"       => 300,
            "expire"  => 1000,
            "urgency" => 1
        );
    }

    private function Connect()
    {
        try
        {
            $this->dbus = new Dbus(Dbus::BUS_SESSION);
            $this->notification = $this->dbus->createProxy(
                "org.freedesktop.Notifications",
                "/org/freedesktop/Notifications",
                "org.freedesktop.Notifications"
            );
            $this->ok = true;
        }
        catch(Exception $e)
        {
            //echo "Couldn't connect to Notifications";
            $this->ok = false;
        }

        return $this->ok;
    }

    public function Notify($array)
    {
        if(!$this->ok)
            if(!$this->Connect())
                return;

        $r = array_merge($this->default, $array);
        try
        {
            $id = $this->notification->Notify(
                $r['name'],
                new DBusUInt32($r['id']),
                $r['icon'],
                $r['title'],
                $r['body'],
                new DBusArray(DBus::STRING, $r['actions']),
                new DBusDict(
                    DBus::VARIANT,
                    array(
                        'x' => new DBusVariant($r['x']),
                        'y' => new DBusVariant($r['y']),
                        'urgency' => new DBusVariant(new DBusByte($r['urgency']))
                    )
                ),
                $r['expire']
            );
        }
        catch(Exception $e)
        {
            //echo "Couldn't send the Notification";
            $this->ok = false;
        }
    }
}

?>
