<?php

class ModuleDate extends Module
{
    private $format;

    public function __construct($format = "D M d, H:i:s")
    {
        $this->format = $format;
        //date_default_timezone_set('localtime');
    }

    public function Update()
    {
        parent::Update();
        $this->output = date($this->format);
    }
}

?>
