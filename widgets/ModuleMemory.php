<?php

class ModuleMemory extends Module
{
    private $format;

    public function __construct($format = "%")
    {
        parent::__construct("Memory");
        $this->format = $format;
    }

    public function Update()
    {
        parent::Update();
        $info = file_get_contents("/proc/meminfo");
        $data = array();
        foreach(explode("\n", $info) as $line)
        {
            if(empty($line))
                continue;
            $res = explode(':', $line);
            $mem = trim($res[1]);
            $data[strtolower($res[0])] = substr($mem, 0, strpos($mem, ' '));
        }
        //$data["memused"] = $data['memtotal'] - $data['memfree'];

        $this->output = ceil($data["active"] / $data['memtotal'] * 100)."%";
    }
}

?>
