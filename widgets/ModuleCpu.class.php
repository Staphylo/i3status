<?php

class ModuleCpu extends Module
{
    private $cpuid;
    private $round;
    private $old;

    public function __construct($cpuid = "", $round = 0)
    {
        parent::__construct("Cpu");
        if(!is_array($cpuid))
            $this->cpuid = array($cpuid);
        else
            $this->cpuid = $cpuid;
        $this->round = $round;
    }

    public function Update()
    {
        parent::Update();
        $m = array();
        $new = array();
        $f = fopen("/proc/stat", "r");
        if($f)
        {
            while(($s = fgets($f)) !== false)
            {
                if(preg_match("#^cpu(\d*) *(\d+) (\d+) (\d+) (\d+)#", $s, $m))
                {
                    if(in_array($m[1], $this->cpuid))
                    {
                        $i = $m[2] + $m[3] + $m[4];
                        $new[$m[1]]['use'] = $i;
                        $new[$m[1]]['total'] = $i + $m[5];
                    }
                }
            }
            fclose($f);
        }

        $i = 0;
        $res = array();
        if(!empty($this->old))
        {
            foreach($this->cpuid as $c)
            {
                $i = round(
                    ($new[$c]['use'] - $this->old[$c]['use']) /
                     ($new[$c]['total'] - $this->old[$c]['total']) * 100,
                    $this->round
                );
                $res[] = $i."%";
            }
        }

        $this->output = implode(' ', $res);
        $this->old = $new;
    }
}

?>
