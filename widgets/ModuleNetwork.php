<?php

class ModuleNetwork extends Module
{
    private $interface;

    public function __construct($interface = array())
    {
        parent::__construct("Network");
        if (!is_array($interface))
            $this->interface = array($interface);
        else
            $this->interface = $interface;
    }

    public function Update()
    {
        parent::Update();

        $have = false;
        $res = file_get_contents("/proc/net/arp");
        foreach (explode("\n", $res) as $line)
        {
            $matches = array();
            preg_match(
                "#([^ ]+)[\t ]+([^ ]+)[\t ]+([^ ]+)[\t ]+([^ ]+)[\t ]+([^ ]+)[\t ]+([^ ]+)#",
                $line,
                $matches
            );
            if (empty($matches))
                break;
            if (in_array($matches[6], $this->interface))
            {
                $this->output = $matches[1]." (".$matches[6].")";
                $have = true;
            }
        }

        if ($have)
            $this->color = "#00FF00";
        else
        {
            $this->color = "#EE2211";
            $this->output = "None";
        }
    }
}

?>
