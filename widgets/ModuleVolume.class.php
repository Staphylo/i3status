<?php

class ModuleVolume extends Module
{
    private $channel;

    public function __construct($channel = "Master")
    {
        parent::__construct("Volume");
        $this->channel = $channel;
    }

    public function Update()
    {
        parent::Update();
        $res = exec("amixer get ".$this->channel);
        $matches = array();
        preg_match("#\[([\d]+\%)\].*\[(on|off)\]#", $res, $matches);

        if(empty($matches))
            return;

        if($matches[2] == "on")
        {
            $this->output = $matches[1];
            $this->color  = "#B0C4DE";
        }
        else
        {
            $this->output = "Mute";
            $this->color  = "#888888";
        }
    }
}

?>
