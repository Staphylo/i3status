<?php

class ModuleMpd extends Module
{
    private $ip;
    private $port;
    private $password;
    private $socket;
    private $data = array();
    private $length = 40;
    private $lastquery = 0;
    private $querydelay;
    private $reconnectdelay = 30.0;
    private $connected = false;
    private $lasttryconnect;
    private $dofade = false;
    private $fade = array(255, 255, 255);

    public function __construct($ip, $port, $password)
    {
        parent::__construct("Mpd");

        $this->ip = $ip;
        $this->port = $port;
        $this->password = $password;
        $this->querydelay = $this->delay;

        $this->Connect();
    }

    public function __destruct()
    {
        if($this->socket)
        {
            $this->Send("close");
            socket_close($this->socket);
        }
    }

    private function Connect()
    {
        if($this->socket)
            socket_close($this->socket);

        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (!$this->socket)
            return false;

        $result = socket_connect($this->socket, $this->ip, $this->port);
        if (!$result)
            return false;

        socket_set_option($this->socket,
            SOL_SOCKET,
            SO_RCVTIMEO,
            array("sec" => 0, "usec" => 100000)
        );

        $this->Read();

        if($this->password)
            $this->Send("password \"".$this->password."\"");

        $this->connected = true;

        return true;
    }

    private function Read()
    {
        if(!$this->socket)
            return "";

        return socket_read($this->socket, 2048);
    }

    private function Send($query)
    {
        if(!$this->socket)
            return;

        return socket_write($this->socket, $query."\n");
    }

    private function Fetch($query)
    {
        if($this->Send($query) === false)
        {
            $this->connected = false;
            return false;
        }

        $result = $this->Read();
        if($result === false)
        {
            $this->connected = false;
            return false;
        }

        return $result;
    }

    public function SetQueryDelay($delay)
    {
        $this->querydelay = $delay;
        return $this;
    }

    public function SetMaxLength($len)
    {
        $this->length = $len;
        return $this;
    }

    public function SetColorFade($bool)
    {
        $this->dofade = $bool;
    }

    private function RetrieveInfo()
    {
        $status = $this->Fetch("status");
        if($status === false)
            return false;
        $song = $this->Fetch("currentsong");
        if($song === false)
            return false;

        $result = $status.$song;
        $this->data = array();
        foreach(explode("\n", $result) as $line)
        {
            $pos = strpos($line, ':');
            $this->data[strtolower(trim(substr($line, 0, $pos)))] =
                trim(substr($line, $pos+2));
        }

        if($this->data["state"] != "stop")
        {
            $this->data["time"] = gmdate("i:s", (int)$this->data["time"]);
            $this->data["elapsed"] = gmdate("i:s",(int)$this->data["elapsed"]);
        }

        return true;
    }

    private function Refresh()
    {
        if($this->data["state"] != "stop")
        {
            $this->SetDisplay(true);

            if ($this->data["state"] == "play")
            {
                if($this->dofade)
                {
                    $dest = array(
                        mt_rand(20, 256),
                        mt_rand(20, 256),
                        mt_rand(20, 256)
                    );

                    for($i = 0; $i < 3; $i++)
                        $this->fade[$i] += ($dest[$i] - $this->fade[$i]) % 10;
                    $this->SetColorRgb($this->fade);
                }
                else
                    $this->SetColorHtml("#FFFFFF");
                $icon = "|>";
            }
            else
            {
                $this->SetColorHtml("#888888");
                $icon = "||";
            }
            //$icon = ($this->data["state"] == "pause") ? "||" : "|>";
            $time = "(".$this->data["elapsed"]."/".$this->data["time"].")";
            $format = "";
            if(isset($this->data["title"]))
            {
                if(isset($this->data["artist"]))
                    $format .= $this->data["artist"]." - ";
                $format .= $this->data["title"];
            }
            else if(isset($this->data["file"]))
            {
                $info = pathinfo($this->data["file"]);
                $format .= $info["filename"];
            }
            $len = strlen($format);
            $padding = min(strlen($format), $this->length - strlen($time));

            if($len > $padding)
            {
                $c = 0;
                $str = "";
                $format .= " - ";
                $len += 3;
                $start = $this->counter % $len;
                for($i = $start; $i < $len && $c < $padding; $i++, $c++)
                    $str .= $format[$i];
                for($i = 0; $i < $start && $c < $padding; $i++, $c++)
                    $str .= $format[$i];
                $format = utf8_encode($str);
            }

            $this->output = $icon." ".$format." ".$time;
        }
        else
        {
            $this->SetDisplay(false);
            $this->output = "[] Stopped";
        }
    }

    public function Update()
    {
        parent::Update();
        if(!$this->connected)
        {
            $this->SetDisplay(false);
            if(microtime(true) - $this->lasttryconnect < $this->reconnectdelay)
                return;
            else if(!$this->Connect())
            {
                $lasttryconnect = microtime(true);
                return;
            }
        }

        if($this->lastquery + $this->querydelay < microtime(true))
        {
            $this->lastquery = microtime(true);
            if(!$this->RetrieveInfo())
                return;
        }

        $this->Refresh();
    }
}

?>
