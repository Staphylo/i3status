<?php

class ModuleBattery extends Module
{
    private $acpi = "/sys/class/power_supply/BAT0";
    private $round;
    private $previous;
    private $gradfull = array(255, 0, 0);
    private $gradlow  = array(0, 255, 0);

    public function __construct($round = 0)
    {
        parent::__construct("Battery");
        $this->round = $round;
        $this->previous = 0;
    }

    private function Notify($c, $pc)
    {
        global $notifier;

        if($pc != $this->previous)
        {
            $res = array('title' => $this->label);

            if($c == '-' && $pc <= 25)
            {
                if($pc == 5)
                {
                    $res['body'] = '<b>Only '.$pc.'% left !!!</b>';
                    $res['urgency'] = 2;
                    $res['expire'] = 0;
                    $notifier->Notify($res);
                }
                if($pc == 10)
                {
                    $res['body'] = '<b>'.$pc.'% left, take your adapter !</b>';
                    $res['urgency'] = 2;
                    $res['expire'] = 0;
                    $notifier->Notify($res);
                }
                if($pc == 25)
                {
                    $res['body'] = '<b>Running low</b>';
                    $res['urgency'] = 1;
                    $res['expire'] = 5000;
                    $notifier->Notify($res);
                }
            }

            if($c == '+')
            {
                if($pc == 100)
                {
                    $res['body'] = "Full";
                    $res['urgency'] = 0;
                    $notifier->Notify($res);
                }
            }
        }
    }

    public function Update()
    {
        parent::Update();

        // Information gathering
        $content = file_get_contents($this->acpi."/uevent");
        $values = array();
        foreach(explode("\n", $content) as $line)
        {
            $tmp = explode("=", $line);
            $values[$tmp[0]] = (isset($tmp[1])) ? $tmp[1] : "";
        }

        if(empty($values))
            return;

        $pc = round($values["POWER_SUPPLY_CHARGE_NOW"] /
            $values["POWER_SUPPLY_CHARGE_FULL"] * 100, $this->round);
        $c = ($values["POWER_SUPPLY_STATUS"] == "Discharging") ? '-' : '+';
        $this->Notify($c, $pc);
        $this->output = $pc."% (".$c.")";

        if($this->previous != $pc)
        {
            // color gradient
            $cur = array();
            for($i = 0; $i < 3; $i++)
                $cur[$i] = $this->gradfull[$i] +
                    (($this->gradlow[$i] - $this->gradfull[$i]) / 100 * $pc)
                * (1 + (100 - $pc) / 100);
            $this->SetColorRgb($cur);

            if($pc <= 10)
                $this->SetBlink(true);
            else
                $this->SetBlink(false);
        }

        $this->previous = $pc;
    }
}

?>
