<?php

abstract class Module
{
    protected $delay = 1.0;
    protected $display = true;
    protected $last = 0;
    protected $output;
    protected $label;
    protected $counter = 0;
    protected $color = "#FFFFFF";
    protected $blink = false;
    protected $blinkcolor = "#333333";
    static private $labelsize = false;

    static public function LabelSize($size)
    {
        self::$labelsize = $size;
    }

    public function __construct($label)
    {
        $this->label = $label;
    }

    public function NeedUpdate($time)
    {
        if ($time + $this->delay - $this->last > 2 * $this->delay)
            return (true);
        return ($this->last + $this->delay < $time);
    }

    public function Update()
    {
        $this->last = microtime(true);
        $this->counter++;
    }

    public function SetDelay($delay)
    {
        $this->delay = $delay;
        return $this;
    }

    public function GetDelay()
    {
        return $this->delay;
    }

    public function SetDisplay($value)
    {
        $this->display = (bool)$value;
        return $this;
    }

    public function ToggleDisplay()
    {
        $this->display = !$this->display;
        return $this;
    }

    public function Display()
    {
        return $this->display;
    }

    public function SetColorRgb($c = array(255, 255, 255))
    {
        $this->color = sprintf("#%02X%02X%02X", $c[0], $c[1], $c[2]);
    }

    public function SetColorHtml($color = "#FFFFFF")
    {
        $this->color = $color;
    }

    public function GetColor()
    {
        if($this->blink && $this->counter % 2 == 0)
            return $this->blinkcolor;
        return $this->color;
    }

    public function SetBlink($bool, $color = "#333333")
    {
        $this->blink = $bool;
        if($this->blink)
            $this->blinkcolor = $color;
    }

    public function GetLabel()
    {
        return $this->label;
    }

    private function Label()
    {
        if(!empty($this->label) && self::$labelsize > 0)
            return substr($this->label, 0, self::$labelsize).": ";
        return "";
    }

    public function __toString()
    {
        return $this->Label().$this->output;
    }
}

?>
