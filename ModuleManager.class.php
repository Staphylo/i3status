<?php

class ModuleManager
{
    private $modules = array();
    private $mintime = 1.0;
    private $labelsize = false;
    private $formated = false;

    public function Add($module)
    {
        $this->modules[] = $module;
        $this->mintime = min($this->mintime, $module->GetDelay());
        return $this;
    }

    public function FormatedOutput($bool)
    {
        $this->formated = $bool;
    }

    public function Run()
    {
        if(empty($this->modules))
            return;

        if($this->formated)
            echo "{\"version\":1}\n[\n";

        while(true)
        {
            //$start = microtime(true);
            $output = array();
            foreach($this->modules as $m)
            {
                if($m->NeedUpdate(microtime(true)))
                    $m->Update();
                if($m->Display())
                {
                    if($this->formated)
                        $output[] = '{"name":"'.$m->GetLabel().'","color":"'.$m->GetColor().'","full_text":"'.$m.'"}';
                    else
                        $output[] = $m;
                }
            }
            if($this->formated)
                echo "[".implode(",", $output)."]\r,";
            else
                echo implode(" | ", $output)."\r";
            usleep($this->mintime * 1000000);
            //usleep($this->mintime * 1000000 - (microtime(true) - $start));
        }
    }
}

?>
