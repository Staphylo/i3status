#!/usr/bin/php -q
<?php

chdir(dirname($argv[0]));
srand(time(NULL));

error_reporting(E_ALL);
ini_set('log_errors', true);
ini_set('error_log', '/tmp/status.log');
ini_set('display_errors', false);

if (extension_loaded("dbus"))
    require_once "Notifier.class.php";
else
    require_once "NoNotifier.class.php";
require_once "Module.class.php";
require_once "ModuleManager.class.php";
foreach(glob("widgets/*.php") as $f)
    require_once $f;

$notifier = new Notifier();
$manager = new ModuleManager();

$mpd = new ModuleMpd("127.0.0.1", 6600, false);
$mpd->SetDelay(0.5);
$mpd->SetQueryDelay(1);
$mpd->SetMaxLength(40);
$mpd->SetColorFade(true);

$vol = new ModuleVolume("Master");
$vol->SetDelay(1);

$cpu = new ModuleCpu();
$cpu->SetDelay(1);

$mem = new ModuleMemory();
$mem->SetDelay(5);

$date = new ModuleDate();
$date->SetDelay(1);

$net = new ModuleNetwork(array("wlan0", "eth0"));
$net->SetDelay(10);

$manager->FormatedOutput(true);
$manager->Add($mpd)
    ->Add($vol)
    ->Add($net)
    ->Add($cpu)
    ->Add($mem)
    ->Add($date);

Module::LabelSize(3);
$manager->Run();

?>
